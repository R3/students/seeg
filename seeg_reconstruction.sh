#!/bin/bash
# Loic MARX
# 2019
# 2019-08-01

echo "*** Reconstruction of SEEG electrodes trajectory from post CT images ***"

# This script is suitable for SEEG electrodes and requires SNAP ITK and Convert3d (c3d) tool
# Segmentation of SEEG electrodes without artifacts from CT 
c3d ct_post.nii.gz -threshold 1000 3072 3 -1 -o seeg_body.nii.gz

# Capture the extremity of the  SEEG electrodes from CT and assigned it to 2. 
c3d ct_post.nii.gz -threshold 4000 7000 2 -1 -o seeg_extremity.nii.gz

# Merge two outputs to get consistent electrode representation 
c3d seeg_body.nii.gz seeg_extremity.nii.gz -add -o seeg_electrode.nii.gz


# Delete the brain skull to get only the electrode (method 1)
# Do the brain mask in binary image 
c3d brain_mask.nii.gz -replace 1 1 -o brain_mask_seeg.nii.gz
# Setup value in the brain mask 
c3d seeg_electrode.nii.gz -replace 2 1 -o DE_seeg.nii.gz
c3d DE_seeg.nii.gz -replace -2 0 DE_2_seeg.nii.gz # get 0 and 1: bone 

# Add brainMask to get the electrodes inside the brain 
c3d DE_2_seeg.nii.gz brain_mask_seeg.nii.gz -add -o DE_mask.nii.gz
# Replace value to setup brain area as background (= 0) and get electrodes inside brain
c3d DE_mask.nii.gz -replace 1 0 -o elec_inside.nii.gz

# Add extrem part of electrodes 
 c3d elec_inside.nii.gz seeg_extremity.nii.gz -add -o electrode_without_skullbone.nii.gz

# Re-thresholding image to correctly define background and electrodes 
c3d electrode_without_skullbone.nii.gz -threshold 0 6 1 0 -o final_seeg.nii.gz


# Additional method to get rid of the skull bone (Method 2)
# Define the skull bone based on CT scan  
c3d ct_post.nii.gz -threshold 900 1400 0 1 -o skull_seeg.nii.gz 

#Combine external part of lead and internal part of leads (skull bone brain still need to be better removed)
c3d skull_seeg.nii.gz DE_2_seeg.nii.gz -add -o skull_DE.nii.gz

# Setup overlapping image as background 
c3d skull_DE.nii.gz -replace 1 0 -o skull_DE.nii.gz

# Combine with the previous SEEg segmentation obtained with Method 1 
c3d skull_DE.nii.gz final_seeg.nii.gz -add -o combine_tworepresentation.nii.gz

# Setup overlapping image as a background to get better rid of remaining skull brain
c3d combine_tworepresentation.nii.gz -threshold 0 2 0 1 -o final_combination.nii.gz
