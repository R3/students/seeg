% The SEEG algorithm: sEEG_Segmentation_Script.m
%
% Purpose:
%     - creating an algorithm that reconstructs SEEG electrodes within the
%     brain
%
% Authors:
%     - Loic Marx & Marie Bofferding, november 2019

%% load the data (CT and BrainMask)
CT_post = NiftiMod('seeg_seg_bul_inside_binary.nii.gz');
Brain_mask = NiftiMod('brain_mask_seeg_bulgarian.nii');
brainMaskImg = Brain_mask.img;

%% get nifti img to binary img, so we can apply some functions
binCTImg = CT_post.img;

% Expand the brain mask to get head bolts of electrodes and get rid of
% artefacts around brain (pre-selection of candidate)
% Step 1 
[croppedImg, expBM] = expandBrainMask(binCTImg, brainMaskImg);

% Compute the connected components of the new expanded brain mask
% cc = bwconncomp(binCTImg,26); % 26 for 3D images (plotting the artefacts)
cc = bwconncomp(croppedImg,26);
disp([num2str(cc.NumObjects) ' potential metal components detected within brain.']);


% Sort out the area and idex
ccProps = regionprops(cc, CT_post.img, 'Area', 'PixelIdxList', 'PixelList', 'PixelValues', 'BoundingBox', 'Centroid'); % TODO not really needed as that info is already in cc
[areas, idxs] = sort([ccProps.Area], 'descend'); % sort by size

% number of index of electrodes
elecIdxs = [];

%% CONSTANTS
% for the contact region (from the paper)
% contact region areas have defined interval of voxels, because they are the smallest parts in the data
MINVOXELNUMBER_CONTACTS = 4; % at least 4 as minimum value
%MAXVOXELNUMBER_CONTACTS = 50; % center 1 
MAXVOXELNUMBER_CONTACTS = 150; % center 2 
% for head
% bolt head areas have defined minimum of voxels, as they are bigger than contact regions
% a maximum is also defined, as there are sometimes cable artifacts left in the data, so the heads are not the
% biggest areas in the data
MINVOXELNUMBER_HEAD = 200; 
MAXVOXELNUMBER_HEAD = 20000;

% define areas of contact regions and heads
% TODO: define areas at beginning of code
contactCandidate = areas(areas >= MINVOXELNUMBER_CONTACTS & areas <= MAXVOXELNUMBER_CONTACTS);
headCandidate = areas(areas >= MINVOXELNUMBER_HEAD & areas <= MAXVOXELNUMBER_HEAD);

% get all possible candidates
%componentIdxs = idxs((areas >= MINVOXELNUMBER_CONTACTS) &( areas <= MAXVOXELNUMBER_CONTACTS) | (areas >= MINVOXELNUMBER_HEAD) & (areas <= MAXVOXELNUMBER_HEAD));

componentIdxsContacts = idxs(areas >= MINVOXELNUMBER_CONTACTS & areas <= MAXVOXELNUMBER_CONTACTS);
componentIdxsHeads = idxs(areas >= MINVOXELNUMBER_HEAD & areas <= MAXVOXELNUMBER_HEAD);

%% PCA to get biggest eigenvalue from latent and principle direction from coeff matrix

newPositionsContactCandidates = zeros(3,length(contactCandidate));
newHeadPositions = zeros(3, length(headCandidate));

% Compute the PCA for head candidate 

for i = 1:(length(headCandidate))
    [coeff,~,latent] = pca(ccProps(componentIdxsHeads(i)).PixelList .* repmat(CT_post.voxsize', length(ccProps(componentIdxsHeads(i)).PixelList) ,1));
    if(length(latent) < 3)
        continue
    end
    latent = sqrt(latent) * 2; % to get full direction
    
    % get centroids of the heads to shift coordinate system to and get starting point for electrode line
    centroidPositionHead = ccProps(componentIdxsHeads(i)).Centroid;
    
    disp(['centroid at ' num2str(i) ' is: ' num2str(centroidPositionHead)]);
    
    % get the centroids of the contactCandidates and shift them by the
    % vector of the centroid of the current head candidate
    for j = 1:(length(contactCandidate))
        centroidPositionContact = ccProps(componentIdxsContacts(j)).Centroid;
        %newContactPosition = coeff * centroidPositionContact' + centroidPositionHead';
        
        %disp(['new Contact Position at ' num2str(j) ' is: ' num2str(newContactPosition')]);
        
        newPositionsContactCandidates(:,j) = centroidPositionContact';
    end
    
    % just for plotting purposes
    newHeadPositions(:,i) = centroidPositionHead';
    
    % compute the centroid of all contact points together to later get the direction in which we define our
    % estimated electrode
    centroidOfAllContacts = mean(newPositionsContactCandidates, 2);
    
    % new origin is centroid point of head and new (local) coordinate system is coeff from the pca
    
    % compute vector between the two points
    vectorBetweenHeadCentroidCentroidOfAll = centroidOfAllContacts - centroidPositionHead';
    
    % compute both "end points" on the bolt head, were the x-axis of our new coordinate system is going through
    % then we can use them to determine, to which end the mean is nearer and therefore we now on which side of
    % the bolt head the contact candidates lie
    endOneOfBoltHead = coeff(:,1) * latent(1,:) + centroidPositionHead';
    endTwoOfBoltHead = coeff(:,1) * latent(1,:) * (-1.0) + centroidPositionHead';
    
    % compute the lengths of the vectors between both endpoints and the centroid point of the contact candidates
    distMeanToEndOne = norm(centroidOfAllContacts - endOneOfBoltHead);
    distMeanToEndTwo = norm(centroidOfAllContacts - endTwoOfBoltHead);
    
    % we set the origin of our future line to the end which is furthest away from the centroid point and go
    % with it in the direction of the other endpoint, because now we know, there are all the contact candidates
    if(distMeanToEndOne < distMeanToEndTwo)
        originOfLine = endTwoOfBoltHead;
        secondPointOnLine = endOneOfBoltHead;
        
    else
        originOfLine = endOneOfBoltHead;
        secondPointOnLine = endTwoOfBoltHead;
    end
    
    % can this be 3-dimensional
    % baseline = polyfit(originOfLine, secondPointOnLine, 1);
    
    % vector pointing from origin point of line to second one
    originToSecondPoint = secondPointOnLine - originOfLine;
    lengthBetweenPoints = norm(originToSecondPoint);
    % get unit vector along the line
    originToSecondPoint = originToSecondPoint / lengthBetweenPoints;
    % compute y = a * x + b to get the line
    % description of our baseline going out from one end of the bolt through the other end into the head space
    % with the contact candidates
    baseline = @(t)(originOfLine + originToSecondPoint * t);
 
      
    figure
    axis([0 500 0 500 0 500])
    axis equal
    scatterMatrix3(newPositionsContactCandidates);
    isosurface(binCTImg, 'blue');
    isosurface(expBM, 'yellow');
    isosurface(brainMaskImg, 'red');
    isosurface(croppedImg,'orange')
    alpha(0.3);
    hold on
    scatterMatrix3(centroidOfAllContacts');
    scatterMatrix3(thresholdElecMaxValue', {}, '*');
    scatterMatrix3(endOneOfBoltHead');
    scatterMatrix3(endTwoOfBoltHead');

end

scatterMatrix3(newHeadPositions');
hold off
