function [croppedImg, expBM] = expandBrainMask(binCTImg, brainMaskImg)

% expand the brain mask to threshold the image so we don't get artifacts
% from everything that it causing them outside of
% the brain, besides the electrode head candidates

% CT_post = NiftiMod('seeg_electrode_additional_tryout_high_threshold_2.nii.gz');
% Brain_mask = NiftiMod('brain_mask.nii.gz');
%brainMaskImg = Brain_mask.img;

structEl = strel('sphere', 5);
expndBrainMask = imdilate(brainMaskImg, structEl);

%isosurface(expndBrainMask);
%isosurface(expndBrainMask,'yellow');

% croppedImg = image .* expndBrainMask;
 croppedImg = binCTImg .* expndBrainMask;
 expBM = expndBrainMask;
 
end